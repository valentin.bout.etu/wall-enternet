#ifndef __HTTP_FILE__
#define __HTTP_FILE__

#include "socket.h"
#include "constant.h"
#include "mime.h"

char *fgets_or_exit(char *buffer, int size, FILE *stream);
void skip_headers(FILE *client);
FILE *check_and_open(const char *target, const char *document_root);
int get_file_size(int fd);
int copy(FILE *in, FILE *out);

#endif
