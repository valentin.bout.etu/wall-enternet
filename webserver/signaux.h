#ifndef __HTTP_SIG__
#define __HTTP_SIG__

#include "socket.h"

void initialiser_signaux(void);
void traitement_signal(int sig);

#endif
