#ifndef __SOCKET_H__
#define __SOCKET_H__

#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <resolv.h>
#include <stdio.h>
#include <sys/un.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>

/** Crée une socket serveur qui écoute sur toute les interfaces IPv4
de la machine sur le port passé en paramètre. La socket retournée
doit pouvoir être utilisée directement par un appel à accept.
La fonction retourne -1 en cas d'erreur ou le descripteur de la
socket créée. */

int creer_serveur(int port);
#endif