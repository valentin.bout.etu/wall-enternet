#ifndef __HTTP_RESPONSE__
#define __HTTP_RESPONSE__

#include "socket.h"
#include "constant.h"
#include "stats.h"
#include "char_utile.h"
#include "file.h"

void send_response_message(FILE *client, int code, const char * reason_phrase, const char *message_body);
void send_response_file(FILE *client, FILE *to_send, const char * target, int code, const char * reason_phrase);
void send_stats(FILE *client);

#endif
