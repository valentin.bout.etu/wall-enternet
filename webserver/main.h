#ifndef __HTTP_MAIN__
#define __HTTP_MAIN__

#include "socket.h"
#include "../lib/parselib/http_parse.h"
#include "constant.h"
#include "stats.h"
#include "signaux.h"
#include "response.h"
#include "file.h"
#include "mime.h"
#include "char_utile.h"
#include "connexion.h"

#endif
