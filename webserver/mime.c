#include "mime.h"

char * get_mime_type_from_extension(const char * extension) {
    if (strcmp(extension, "html") == 0 || strcmp(extension, "htm") == 0 || strcmp(extension, "shtml") == 0) return "text/html";
    if (strcmp(extension, "png") == 0) return "image/png";
    if (strcmp(extension, "jpg") == 0 || strcmp(extension, "jpeg") == 0 || strcmp(extension, "jpe") == 0 ) return "image/jpg" ;
    if (strcmp(extension, "gif") == 0) return "image/gif";
    if (strcmp(extension, "css") == 0) return "text/css";
    if (strcmp(extension, "ico") == 0) return "image/vnd.microsoft.icon";
    if (strcmp(extension, "svg") == 0 || strcmp(extension, "svgz") == 0 ) return "image/svg+xml";
    return NULL;
}
