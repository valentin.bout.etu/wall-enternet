#include "response.h"

void send_status(FILE *client, int code, const char *reason_phrase) {
    fprintf(client, "%s %d %s%s", HTTP_VERSION, code, reason_phrase, CRLF);
}

void send_response_message(FILE *client, int code, const char * reason_phrase, const char *message_body) {
    modifier_statistiques(code);
    send_status(client, code, reason_phrase);
    fprintf(client, "Connection: keep-alive%s", CRLF);
    fprintf(client, "Content-Length: %ld%s", strlen(message_body), CRLF);
    fprintf(client, "%s", CRLF);
    fprintf(client, "%s", message_body);
}

void send_response_file(FILE *client, FILE *to_send, const char * target, int code, const char * reason_phrase) {
    char * filename = get_filename_from_target(target);
    char * extension = get_extension_from_filename(filename);
    modifier_statistiques(code);
    send_status(client, code, reason_phrase);
    fprintf(client, "Connection: close%s", CRLF);
    fprintf(client, "Content-Length: %d%s", get_file_size(fileno(to_send)), CRLF);
    fprintf(client, "Content-Type: %s%s", get_mime_type_from_extension(extension), CRLF);
    fprintf(client, "%s", CRLF);
    copy(to_send, client);
    fprintf(client, "%s", CRLF);
    free(filename);
    free(extension);
}

void send_stats(FILE *client) {
    char * message_body = response_from_stats();
    modifier_statistiques(200);
    send_status(client, 200, "OK");
    fprintf(client, "Connection: keep-alive%s", CRLF);
    fprintf(client, "Content-Length: %ld%s", strlen(message_body), CRLF);
    fprintf(client, "%s", CRLF);
    fprintf(client, "%s", message_body);
    free(message_body);
}
