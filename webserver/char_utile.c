#include "char_utile.h"

char *rewrite_target(char *target) {
    if (strcmp(target, "/") == 0) target = "/index.html";
    char *temp; 
    temp = strchr(target, '?');
    if (temp == NULL) return target;
    *temp = '\0';
    return target;
}

char * get_last_part_after_char(const char * target, const char c) {
    char * temp = strdup(target);
    char * secondTemp;
    while((secondTemp = strchr(temp, c)) != NULL) {
        free(temp);
        if (*(secondTemp +1) != '\0') temp = strdup((secondTemp + 1));
        else temp = strdup("");
    };
    return temp;
}

char * get_filename_from_target(const char * target) {
    return get_last_part_after_char(target, '/');
}

char * get_extension_from_filename(const char * filename) {
    return get_last_part_after_char(filename, '.');
}
