#include "stats.h"
#include "socket.h"

static web_stats * stats;
static sem_t * semaphore;

int init_stats(void) {
    init_semaphore();
    stats = (web_stats *) mmap(NULL, sizeof(web_stats), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
    sem_wait(get_semaphore());
    stats->ko_400 = 0;
    stats->ko_403 = 0;
    stats->ko_404 = 0;
    stats->ok_200 = 0;
    stats->served_connections = 0;
    stats->served_requests = 0;
    sem_post(get_semaphore());
    return 0;
}

web_stats *get_stats(void) {
    return stats;
}

int init_semaphore(void) {
    semaphore = (sem_t *) mmap(NULL, sizeof(sem_t), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
    return sem_init(semaphore, 1, 1);
}

sem_t *get_semaphore(void) {
    return semaphore;
}

void modifier_statistiques(int code) {
    sem_wait(get_semaphore());
    web_stats * stats = get_stats();
    stats->served_connections += 1;
    switch (code) {
        case 200:
            stats->ok_200 += 1;
            break;
        case 400:
            stats->ko_400 += 1;
            break;
        case 404:
            stats->ko_404 += 1;
            break;
        case 403:
            stats->ko_403 += 1;
            break;
    }
    sem_post(get_semaphore());
}

void modifier_statistiques_increment_served_requests() {
    sem_wait(get_semaphore());
    get_stats()->served_requests += 1;
    sem_post(get_semaphore());
}

char * response_from_stats() {
    sem_wait(get_semaphore());
    web_stats * stats = get_stats();
    int served_connections = stats->served_connections;
    int served_requests = stats->served_requests;
    int ok_200 = stats->ok_200;
    int ko_400 = stats->ko_400;
    int ko_403 = stats->ko_403;
    int ko_404 = stats->ko_404; 
    sem_post(get_semaphore());

    /* "Server connection: \nServer requests: \nServer 200 rep: \nServer 400 rep: \nServer 403 rep: \nServer 404 rep: \n
    " */
    int size_buffer = 113 + ok_200 * sizeof(int) + ko_400  * sizeof(int) + ko_403  * sizeof(int) + ko_404  * sizeof(int) + served_connections  * sizeof(int) + served_requests  * sizeof(int);
    char * message_body = (char *) malloc(size_buffer * sizeof(char));
    snprintf(message_body, size_buffer, "Server connection: %d\nServer request: %d\nServer 200 rep: %d\nServer 400 rep: %d\nServer 403 rep: %d\nServer 404 rep: %d\n", served_connections, served_requests, ok_200,ko_404, ko_403, ko_404 );
    return message_body;
}
