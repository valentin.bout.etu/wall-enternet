#ifndef __HTTP_MIME__
#define __HTTP_MIME__

#include <string.h>

char * get_mime_type_from_extension(const char * extension);

#endif
