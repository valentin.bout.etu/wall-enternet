#include "connexion.h"

int handleConnection(int socket_serveur, const char * document_root)
{
    int socket_client = accept(socket_serveur, NULL, NULL);
    if (socket_client == -1)     
    {
        perror("accept");
        exit(1);
    }
    pid_t pid = fork();
    if (pid == -1) 
    {
        perror("fork");
        exit(1);
    } else if (pid == 0)
    {

        char * options = "a+";
        FILE * f = fdopen(socket_client, options);
        if (f == NULL) {
            printf("error opening \n");
            return 1;
        }
        char buffer[4096] = "";
        http_request htRequest;
        fgets_or_exit(buffer, 4096, f);
        int parse_ret = parse_http_request(buffer, &htRequest);
        skip_headers(f);
        if (parse_ret < 0) {
            if (htRequest.method == HTTP_UNSUPPORTED) send_response_message(f, 405, "Method Not Allowed", "Method Not Allowed\r\n");
            else send_response_message(f, 400, "Bad Request", "Bad request\r\n");

        } else if (strstr(htRequest.target, "/../") != NULL || strstr(htRequest.target, "../") != NULL ) {
            send_response_message(f, 403, "Forbidden", "Forbidden\r\n");
        }else {
            if (strcmp(htRequest.target, "/stats") == 0) send_stats(f);
            else {
                FILE *to_send = check_and_open(rewrite_target(htRequest.target), document_root );
                if (to_send == NULL) send_response_message(f, 404, "Not Found", "Not Found\r\n");
                else {
                    send_response_file(f, to_send, rewrite_target(htRequest.target), 200, "OK");
                    fclose(to_send);
                }
            } 
        }
        modifier_statistiques_increment_served_requests();
        fclose(f);
        close(socket_client);
        exit(0);
    } else {
        // processus papounet
        handleConnection(socket_serveur, document_root);
    }
    return 0;
}
