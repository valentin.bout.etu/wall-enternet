#ifndef __HTTP_CONNEXION__
#define __HTTP_CONNEXION__

#include "main.h"
#include "socket.h"
#include "../lib/parselib/http_parse.h"
#include "constant.h"
#include "stats.h"
#include "signaux.h"
#include "response.h"
#include "file.h"
#include "mime.h"
#include "char_utile.h"

int handleConnection(int socket_serveur, const char * document_root);

#endif
