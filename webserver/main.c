#include "main.h"

int main(int argc, char *argv[])
{
    if (argc <= 2 || strcmp(argv[1], "-d")) {
        printf("Incorrect use of Wall-Ethernet, Please provide a directory with -d options \n");
        return 1;
    }
    struct stat info;
    if (argv[2][strlen(argv[2]) -1] == '/') argv[2][strlen(argv[2]) -1] = '\0';
    int statRC = stat( argv[2], &info );
    if( statRC != 0 )
    {
        if(ENOENT == errno) {
            printf("Incorrect path given, directory not exists \n");
            return 1;
        } else {
            perror("stat");
            exit(1);
        }
    }
    if(!S_ISDIR(info.st_mode)) {
        printf("Incorrect path given, not a directory \n");
        return 1;
    } 

    initialiser_signaux();
    init_stats();
    int socket_serveur = creer_serveur(8080);
    if (socket_serveur < 0 ) 
    {
        return 1;
    }

    return handleConnection(socket_serveur, argv[2]);
}
