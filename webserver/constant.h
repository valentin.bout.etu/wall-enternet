#ifndef __HTTP_CONSTANT__
#define __HTTP_CONSTANT__

#define HTTP_VERSION "HTTP/1.1"
#define CRLF "\r\n"
#define CRLFALT "\n"
#endif
