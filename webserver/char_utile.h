#ifndef __HTTP_CHAR_UTILE__
#define __HTTP_CHAR_UTILE__

#include <string.h>
#include <stdlib.h>
#include "main.h"
char *rewrite_target(char *target);
char * get_last_part_after_char(const char * target, const char c);
char * get_filename_from_target(const char * target);
char * get_extension_from_filename(const char * filename);

#endif
