#ifndef __HTTP_STATS__
#define __HTTP_STATS__

#include <sys/mman.h>
#include <semaphore.h>

typedef struct{
    int served_connections;
    int served_requests;
    int ok_200;
    int ko_400;
    int ko_403;
    int ko_404;
} web_stats;

int init_stats(void);
web_stats *get_stats(void);
int init_semaphore(void) ;
sem_t *get_semaphore(void) ;
void modifier_statistiques(int code) ;
void modifier_statistiques_increment_served_requests();
char * response_from_stats();

#endif
