#include "signaux.h"

void traitement_signal(int sig) {
    printf("Signal recu %d \n", sig);
    while(waitpid(-1, NULL, WNOHANG) > 0);
}

void initialiser_signaux(void) {
    struct sigaction sa;
    sa.sa_handler = traitement_signal;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
    {
        perror("sigaction(SIGCHLD)");
    }
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
    {
        perror("signal");
    }
}
