#include "file.h"

char *fgets_or_exit(char *buffer, int size, FILE *stream) {
    char * stillAlive = fgets(buffer, size, stream);
    if (stillAlive == NULL) exit(1);
    return stillAlive;
}

void skip_headers(FILE *client) {
    char buffer[4096] = "";
    fgets_or_exit(buffer, 4096, client);
    while(strcmp(buffer, CRLF) && strcmp(buffer, CRLFALT)) fgets_or_exit(buffer, 4096, client);
}

FILE *check_and_open(const char *target, const char *document_root) {
    if (target == NULL || document_root == NULL ) return NULL;
    int totalL = strlen(target) + strlen(document_root) + 1;
    char path[totalL];
    memset(path, '\0', totalL * sizeof(char));
    strcat(path, document_root);
    strcat(path, target);
    struct stat path_stat;
    stat(path, &path_stat);
    if(!S_ISREG(path_stat.st_mode)) return NULL;
    return fopen(path, "r");
}

int get_file_size(int fd) {
    struct stat info;
    int fstatB = fstat(fd, &info);
    return fstatB < 0 ? fstatB : info.st_size;
}

int copy(FILE *in, FILE *out) {
    int buff_size = 10240;
    char buffer[buff_size];
    size_t result;
    while (( result = fread( buffer, 1, buff_size, in)) > 0 ) {
        fwrite(buffer, 1, result, out );
    }
    return 0;
}
