# Wall-Enternet

Ce serveur minimaliste ecris en ```C``` a pour but de de servir un dossier via des requetes HTTP GET. 

## Lancement du serveur:
 
Pour lancer le serveur, suivez les etapes suivantes: 
1. Recuperer le projet sur gitlab:
   ```sh
   $ git clone https://gitlab.univ-lille.fr/valentin.bout.etu/wall-enternet.git
   ```
2. Deplacez-vous dans le dossier recupere:
   ```
   $ cd Wall-Enternet
   ```
3. Compiler le programme :
   ```sh
   $ make
   ```
4. Lancer le serveur:
   ```sh
   $ ./webserver/Wall-Enternet -d <chemin du dossier a servir>
   ```
5. Tester le serveur:
   ```sh
   $ firefox localhost:8080
   ```
   Ou directement dans votre navigateur a [cette adresse](http://localhost:8080)

#### Informations: 
1. Une route "speciale" [/stats](http://localhost:8080/stats) permet d'obtenir quelque statistique sur le serveur.
2. Un dossier [website](./website) existe dans la racine du projet afin de tester facilement le serveur avec la commande:
   ```sh
   $ ./webserver/Wall-Enternet -d ./website
   ```` 

## Author: 
1. AUTHORS 1
    - Last Name : **BOUT**
    - First Name : Valentin
    - Email : valentin.bout.etu@univ-lille.fr
2. AUTHORS 2
    - Last Name : **HERBECQ**
    - First Name : Baptiste
    - Email : baptiste.herbecq.etu@univ-lille.fr
    
Voir le fichier: [AUTHORS](./AUTHORS)

## Licence:

Voir le fichier: [LICENSE](./LICENSE)